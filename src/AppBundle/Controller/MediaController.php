<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Media;

class MediaController extends Controller
{
    /**
     * @Route("/media/{filename}", name="media_download")
     */
    public function downloadAction(Media $media)
    {
        $mediaDir = $this->container->getParameter('storage_directory');
        $response = new BinaryFileResponse($mediaDir . DIRECTORY_SEPARATOR . $media->getFilename());
        $response->headers->set('Content-Type', $media->getMimetype());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $media->getOriginalFilename()
        );
        return $response;
    }
}
