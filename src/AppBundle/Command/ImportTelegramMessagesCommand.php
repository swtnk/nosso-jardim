<?php

namespace AppBundle\Command;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use JsonCollectionParser\Parser;

class ImportTelegramMessagesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:import-telegram-messages')
            ->addArgument('file', InputArgument::REQUIRED, 'Telegram messages file');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resolver = $this->getContainer()->get('app.resolver.register_telegram_dumper_message');
        $resolver->setAutoFlush(false);
        
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        
        $file = $input->getArgument('file');

        $mediaDirectory = realpath(dirname($file) . '/../media');

        $parser = new Parser();
        
        $i = 0;
        
        $parser->parse($file, function ($message) use ($resolver, $mediaDirectory, $output, $i, $em) {
            
            try {
                
                $this->cleanMediaPath($message);
                                
                $resolver->resolve(new ParameterBag([
                    'json' => $message,
                    'media_directory' => $mediaDirectory
                ]));
                
                $output->writeln($message['id'] . ' Message successfully imported. ');
                
                $i++;
                
                # bulk inserts
                if (($i % 20) === 0) {
                    $em->flush();
                    $em->clear();
                    # limpa o cache local
                    $resolver->clearCache();
                }             
                
            } catch (\InvalidArgumentException $e) {
                $output->writeln($message['id'] . ' ' . $e->getMessage());
            }
        });
        
        $em->flush();
        $em->clear();

        $output->writeln('The End');
    }
    
    /**
     * @param array $message
     */
    private function cleanMediaPath(&$message)
    {
        if ($message['from']['userPhoto'] ?? null) {
            $message['from']['userPhoto'] = explode('media/', $message['from']['userPhoto'])[1];
        }

        if ($message['media']['file'] ?? null) {
            $message['media']['file'] = explode('media/', $message['media']['file'])[1];
        }
    }
}
