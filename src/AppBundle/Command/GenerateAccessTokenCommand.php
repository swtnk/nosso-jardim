<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateAccessTokenCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:generate-access-token')
            ->addArgument(
                'userId',
                InputArgument::REQUIRED,
                'User id to gererate the token'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        
        $connectService = $this->getContainer()->get('app.service.connect');

        $user = $em->find('AppBundle:User', $input->getArgument('userId'));

        if (empty($user)) {
            throw new \InvalidArgumentException('User not found');
        }

        $token = $connectService->createToken($user);

        $output->writeln("Token: <info>$token</info>");
        $output->writeln('');
        $output->writeln("HTTP header usage example: <info>Authorization: Bearer $token</info>");
        $output->writeln('');
        $output->writeln("Access link: <info>{$connectService->getAccessLink($token)}</info>");
    }
}
