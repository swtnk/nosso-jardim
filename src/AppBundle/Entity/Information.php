<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="information",uniqueConstraints={@ORM\UniqueConstraint(name="subtopic_thread", columns={"subtopic_id","thread_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InformationRepository")
 */
class Information
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Thread
     *
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="informations")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     */
    private $thread;

    /**
     * @var Subtopic
     *
     * @ORM\ManyToOne(targetEntity="Subtopic", inversedBy="informations")
     * @ORM\JoinColumn(name="subtopic_id", referencedColumnName="id")
     */
    private $subtopic;

    /**
     * @var string
     *
     * @ORM\Column(name="about", type="text", nullable=true)
     */
    private $about;

    public function getThread()
    {
        return $this->thread;
    }

    public function getSubtopic()
    {
        return $this->subtopic;
    }

    public function setSubtopic(Subtopic $subtopic) {
        $this->subtopic = $subtopic;
    }

    public function setThread(Thread $thread) {
        $this->thread = $thread;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAbout($about) {$this->about = $about;}
    public function getAbout() {return $this->about;}

}