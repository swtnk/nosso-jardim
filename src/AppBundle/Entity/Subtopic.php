<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Subtopics
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="subtopic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubtopicRepository")
 * @ORM\EntityListeners({"SubtopicListener"})
 */
class Subtopic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Subtopic")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Subtopic", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Subtopic", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="subtopic")
     */
    private $items;

    /** @ORM\Column(type="boolean") */
    private $canHaveItems;
    
    /**
     * @var ArrayCollection|Information[]
     *
     * @ORM\OneToMany(targetEntity="Information", mappedBy="thread")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $informations;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->informations = new ArrayCollection();
        $this->canHaveItems = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setParent(Subtopic $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getSubtopics() {
        return $this->getChildren();
    }
    
    public function getChildren() {
        return $this->children;
    }

    public function addItem(Item $item) {
        $item->setSubtopic($this);
        $this->items->add($item);
    }

    public function getCanHaveItems() {
        return $this->canHaveItems;
    }

    public function setCanHaveItems($canHaveItems) {
        $this->canHaveItems = $canHaveItems;
    }

    public function getAncestors() {
        $ancestors = [];
        $parent = $this->getParent();
        while($parent !== null) {
            $ancestors[] = $parent;
            $parent = $parent->getParent();
        }
        return array_reverse($ancestors);
    }

    public function getItems() {
        return $this->items;
    }

    public function getInformation() {return $this->information;}

    
}