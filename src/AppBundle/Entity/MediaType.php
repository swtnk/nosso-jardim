<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaType
 *
 * @ORM\Table(name="media_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaTypeRepository")
 */
class MediaType
{
    /**
     * @var integer
     */
    const PHOTO = 1;
    
    /**
     * @var integer
     */
    const DOCUMENT = 2;
    
    /**
     * @var integer
     */
    const AUDIO = 3;
    
    /**
     * @var integer
     */
    const VIDEO = 4;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @param integer $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
