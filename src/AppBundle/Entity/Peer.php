<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="peer",uniqueConstraints={@ORM\UniqueConstraint(name="peer_id", columns={"peer_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PeerRepository")
 */
class Peer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="print_name", type="string", length=255)
     */
    private $printName;

    /**
     * @var string
     *
     * @ORM\Column(name="peer_type", type="string", length=255)
     */
    private $peerType;

    /**
     * @var string
     *
     * @ORM\Column(name="peer_id", type="string", length=50)
     */
    private $peerId;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="peer")
     */
    private $messages;

    public function getId()
    {
        return $this->id;
    }

    public function getPeerType() {return $this->peerType;}
    public function setPeerType($peerType) {$this->peerType = $peerType;}
    
    public function getPrintName() {return $this->printName;}
    public function setPrintName($printName) {$this->printName = $printName;}

    public function getPeerId() {return $this->peerId;}
    public function setPeerId($peerId) {$this->peerId = $peerId;}
}