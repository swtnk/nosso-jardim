<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConnectRequest
 *
 * @ORM\Table(name="connect_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConnectRequestRepository")
 */
class ConnectRequest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="update_id", type="string", length=255, unique=true)
     */
    private $updateId;

    /**
     * @var array
     *
     * @ORM\Column(name="json", type="json_array")
     */
    private $json;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * forçar a criação por meio de named constructors
     */
    private function __construct(){}

    /**
     * @param array $payload
     * @return ConnectRequest
     */
    public static function fromPayload(array $payload)
    {
        $self = new ConnectRequest();
        $self->createdAt = new \DateTime();
        $self->updateId = $payload['update_id'];
        $self->json = $payload;
        return $self;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get updateId
     *
     * @return string
     */
    public function getUpdateId()
    {
        return $this->updateId;
    }

    /**
     * Get json
     *
     * @return array
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
