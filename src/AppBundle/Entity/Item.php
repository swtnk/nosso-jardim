<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Items
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemRepository")
 */
class Item
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Area
     *
     * @ORM\ManyToOne(targetEntity="Subtopic", inversedBy="items")
     * @ORM\JoinColumn(name="subtopic_id", referencedColumnName="id", nullable=false)
     */
    private $subtopic;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getArea()
    {
        return $this->area;
    }

    public function setArea(Area $area)
    {
        $this->area = $area;
    }

    public function setSubtopic(Subtopic $subtopic) {
        $this->subtopic = $subtopic;
    }

    public function getSubtopic() {
        return $this->subtopic;
    }
}
