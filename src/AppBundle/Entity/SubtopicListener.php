<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;

class SubtopicListener
{
	public function preRemove(Subtopic $subtopic, LifecycleEventArgs $event)
    {
        if($subtopic->getItems()->count() > 0) {
        	throw new \AppBundle\Exceptions\UserErrorException("Um subtópico com items não pode ser removido.", 1);
        }

    }
}