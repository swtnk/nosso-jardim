<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id", nullable=false)
     */
    private $sender;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Peer", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="peer_id", referencedColumnName="id", nullable=false)
     */
    private $peer;

    /**
     * @var Thread
     *
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="messages")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id", nullable=true)
     */
    private $thread;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var bool
     *
     * @ORM\Column(name="irrelevant", type="boolean", nullable=false)
     */
    private $irrelevant;

    /**
     * @var string
     *
     * @ORM\Column(name="telegram_id", type="string", length=190, nullable=true, unique=true)
     */
    private $telegramId;

    /**
     * @var array
     *
     * @ORM\Column(name="json", type="json_array")
     */
    private $json;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime")
     */
    private $sentAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var Media
     * 
     * @ORM\ManyToOne(targetEntity="Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", nullable=true)
     */
    private $media;

    /**
     * @param User $sender
     * @param \DateTime $sentAt
     * @param string|null $text
     */
    public function __construct(
        User $sender,
        \DateTime $sentAt,
        Peer $peer,
        $text = null
    ) {
        $this->sender = $sender;
        $this->text = $text;
        $this->sentAt = $sentAt;
        $this->peer = $peer;
        $this->irrelevant = false;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $telegramId
     * @return $this
     */
    public function setTelegramId($telegramId)
    {
        $this->telegramId = $telegramId;
        return $this;
    }

    /**
     * @param json $json
     * @return $this
     */
    public function setJson(array $json)
    {
        $this->json = $json;
        return $this;
    }

    /**
     * @param Media $media
     * @return $this
     */
    public function setMedia(Media $media)
    {
        $this->media = $media;
        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
        
    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Get thread
     *
     * @return Thread|null
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Get json
     *
     * @return array
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getTelegramId()
    {
        return $this->telegramId;
    }

    /**
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @return bool
     */
    public function isIrrelevant()
    {
        return $this->irrelevant;
    }
    /**
     * @return Message
     */
    public function markAsIrrelevant()
    {
        $this->irrelevant = true;
        return $this;
    }
    
    /**
     * @return Message
     */
    public function markAsSignificant()
    {
        $this->irrelevant = false;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIrrelevant()
    {
        return $this->isIrrelevant();
    }

    /**
     * @param Thread $thread
     * @return $this
     */
    public function setThread(Thread $thread)
    {
        $this->thread = $thread;
        return $this;
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return Message
     */
    public function removeThread()
    {
        if ($this->thread) {
            $this->thread->getMessages()->removeElement($this);
            $this->thread = null;
        }

        return $this;
    }

    /**
     * @param Peer $peer
     * @return $this
     */
    public function setPeer(Peer $peer)
    {
        $this->peer = $peer;
        return $this;
    }

    public function getPeer() {return $this->peer;}

    public function clean()
    {
        $this->removeThread();
        $this->markAsSignificant();
    }
}
