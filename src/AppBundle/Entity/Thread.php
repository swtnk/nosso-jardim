<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Thread
 *
 * @ORM\Table(name="thread")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThreadRepository")
 */
class Thread
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection|Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="thread")
     * @ORM\OrderBy({"sentAt" = "ASC"})
     */
    private $messages;

    /**
     * @var ArrayCollection|Information[]
     *
     * @ORM\OneToMany(targetEntity="Information", mappedBy="thread", cascade={"remove"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $informations;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->informations = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="first_sent_at", type="datetime")
     */
    private $firstSentAt;

    /**
     * @param Message $message
     * @return Thread
     */
    public function addMessage(Message $message)
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setThread($this);

            $iterator = $this->messages->getIterator();
            $iterator->uasort(function ($a, $b) {
                return ($a->getSentAt() < $b->getSentAt()) ? -1 : 1;
            });
            $this->messages = new ArrayCollection(iterator_to_array($iterator));
        }

        $hasOldest = false;
        foreach($this->messages as $m) {
            if($m->getSentAt() < $message->getSentAt()) {
                $hasOldest = true;
            }
        }
        if(!$hasOldest) {
            $this->firstSentAt = $message->getSentAt();
        }
        
        return $this;
    }


    /**
     * Get messages
     *
     * @return ArrayCollection|Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return \DateTime
     */
    public function getFirstSentAt()
    {
        return $this->firstSentAt;
    }

    public function addInformation(Information $information) {
        $information->setThread($this);
        $this->informations[] = $information;
    }

    public function getInformations() {return $this->informations;}


 }
