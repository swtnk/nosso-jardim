<?php

namespace AppBundle\Repository;

use Symfony\Component\HttpFoundation\ParameterBag;

class MessageRepository extends AbstractRepository
{
    /**
     * @param ínteger[] $messageIds
     * @return \AppBundle\Entity\Message[]
     * @throws \RuntimeException
     */
    public function findByIds($messageIds)
    {
        $messages = $this->findById($messageIds);

        if (count($messages) !== count($messageIds)) {
            $error = 'Some messages were not found on the system.'
            .' You asked for ids [' . implode(',',$messageIds) . '] but'
            .' we have found only [' . implode(
                ',',
                array_map(
                    function($m){return $m->getId();}, 
                    $messages
                )
            ).']';
            throw new \RuntimeException($error);
        }

        return $messages;
    }
    
    /**
     * @param ParameterBag $filters
     * @return array
     */
    public function getPageInfoData($filters)
    {
        $qb = $this->getSearchQueryBuilder($filters);
        $qb->select('count(m.id)');
        $countWithCursor = $qb->getQuery()->getSingleScalarResult();

        $qb = $this->getSearchQueryBuilder($filters, false);
        $qb->select('count(m.id)');
        $countWithoutCursor = $qb->getQuery()->getSingleScalarResult();
        
        if ($filters['last']) {
            return [
                'hasPreviousPage' => $countWithCursor > $filters['last'],
                'hasNextPage' => $countWithoutCursor > $countWithCursor
            ];
        }
        
        return [
            'hasPreviousPage' => $countWithoutCursor > $countWithCursor,
            'hasNextPage' => $countWithCursor > $filters['first']
        ];       
    }

    /**
     * @param ParameterBag $filters
     * @return \AppBundle\Entity\Message[]
     */
    public function search($filters)
    {
        $qb = $this->getSearchQueryBuilder($filters);
        $results = $qb->getQuery()->getResult();
        if($filters['last']) {
            return array_reverse($results);
        }
        return $results;
    }

    /**
     * @param ParameterBag $filters
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getSearchQueryBuilder($filters, $includeCursorFilters = true)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('m, me')
           ->leftJoin('m.media', 'me');
        
        if ($includeCursorFilters) {
            if ($filters['after']) {
                $qb->andWhere('m.sentAt > :after')
                   ->setParameter('after', $filters['after']);
            }

            if ($filters['afterIncluding']) {
                $qb->andWhere('m.sentAt >= :afterIncluding')
                   ->setParameter('afterIncluding', $filters['afterIncluding']);
            }
            
            if ($filters['before']) {
                $qb->andWhere('m.sentAt < :before')
                   ->setParameter('before', $filters['before']);
            }        
        }
        
        if ($filters['id']) {
            $qb->andWhere('m.id = :id')
               ->setParameter('id', $filters['id']);
        }
        
        if ($filters['threadId']) {
            $qb->andWhere('m.thread = :thread')
               ->setParameter('thread', $filters['threadId']);
        }

        if ($filters['peerId']) {
            $qb->andWhere('m.peer = :peer')
               ->setParameter('peer', $filters['peerId']);
        }
        
        $qb->orderBy('m.sentAt', 'ASC');

        if ($filters['first']) {
            $qb->orderBy('m.sentAt', 'ASC')
               ->setMaxResults($filters['first']);
        }

        if ($filters['last']) {
            $qb->orderBy('m.sentAt', 'DESC')
               ->setMaxResults($filters['last']);
        }    

        return $qb;
    }

    /**
     * @return \AppBundle\Entity\Message
     */
    public function findFirstCleanMessage($peerId = null)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('m');
        $qb->orderBy('m.sentAt', 'ASC');
        $qb->andWhere('m.thread IS NULL');
        $qb->andWhere('m.irrelevant = :irrelevant');
        $qb->setParameter('irrelevant',false);
        if($peerId) {
            $qb->andWhere('m.peer = :peer');
            $qb->setParameter('peer', $peerId);
        }
        $qb->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function oldestMessageDate($peerId=null) {
        $extra = null;
        if($peerId) {
            $extra = " WHERE m.peer = " . $peerId;
        }
        $q = "SELECT m.sentAt FROM AppBundle\Entity\Message m $extra ORDER BY m.sentAt ASC";
        $q = $this->_em->createQuery($q);
        $q->setMaxResults(1);
        return $q->getResult()[0]['sentAt'];
    }

    public function newestMessageDate($peerId=null) {
        $extra = null;
        if($peerId) {
            $extra = " WHERE m.peer = " . $peerId;
        }
        $q = "SELECT m.sentAt FROM AppBundle\Entity\Message m $extra ORDER BY m.sentAt DESC";
        $q = $this->_em->createQuery($q);
        $q->setMaxResults(1);
        return $q->getResult()[0]['sentAt'];
    }

    public function messagesCount($peerId = null) {
        $extra = null;
        if($peerId) {
            $extra = " WHERE m.peer = " . $peerId;
        }
        $q = "SELECT count(m.id) FROM AppBundle\Entity\Message m ".$extra;
        $q = $this->_em->createQuery($q);
        return intval($q->getSingleScalarResult());
    }

    public function cleanMessagesCount($peerId = null) {
        if($peerId) {
            $extra = " AND m.peer = " . $peerId;
        }
        $q = "SELECT count(m.id) FROM AppBundle\Entity\Message m WHERE m.thread IS NULL AND m.irrelevant=false".$extra;
        $q = $this->_em->createQuery($q);
        return intval($q->getSingleScalarResult());
    }

}
