<?php

namespace AppBundle\Repository;

class PeerRepository extends AbstractRepository
{

	public function findAll()
    {
        return $this->findBy(array(), array('printName' => 'ASC'));
    }
    
}
