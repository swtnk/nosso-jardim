<?php

namespace AppBundle\Repository;
use Symfony\Component\HttpFoundation\ParameterBag;

class ThreadRepository extends AbstractRepository
{

    public function search($filters)
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->select('t', 'm')
        ->from('AppBundle\Entity\Thread', 't')
        ->leftJoin('t.messages', 'm');
        
        if ($filters['id']) {
            $qb->andWhere('t.id = :id')
               ->setParameter('id', $filters['id']);
        }

        if ($filters['peerId']) {
            $qb->andWhere('m.peer = :peer')
                ->setParameter('peer', $filters['peerId']);
        }

        $qb->addOrderBy('t.id', 'DESC');
        
        return $qb->getQuery()->getResult();
    }

    public function oldestThreadDate($peerId) {
        $where = $peerId ? "WHERE m.peer = ".$peerId : null;
        $q = "
            SELECT t.firstSentAt 
                FROM AppBundle\Entity\Thread t 
                JOIN t.messages m
                $where
                ORDER BY t.firstSentAt ASC";
        $q = $this->_em->createQuery($q);
        $q->setMaxResults(1);
        return $q->getResult()[0]['firstSentAt'];
    }

    public function newestThreadDate($peerId) {
        $where = $peerId ? "WHERE m.peer = ".$peerId : null;
        
        $q = "
            SELECT t.firstSentAt 
                FROM AppBundle\Entity\Thread t 
                JOIN t.messages m
                $where
                ORDER BY t.firstSentAt DESC";
        $q = $this->_em->createQuery($q);
        $q->setMaxResults(1);
        return $q->getResult()[0]['firstSentAt'];
    }
}
