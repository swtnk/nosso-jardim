<?php
namespace AppBundle\Resolver;

use Symfony\Component\HttpFoundation\ParameterBag;
use Doctrine\ORM\EntityManager;

class MessagesResolver extends EmAwareResolver
{

    public function repo($entityName = 'AppBundle:Message') {
        return parent::repo($entityName);
    }
    
    public function find($filters)
    {
        //** todo: porque alguns filtros aqui e outros no repo? **/
        $repo = $this->repo();
        
        if ($filters['first'] && $filters['last']) {
            throw new \InvalidArgumentException(
                'Including a value for both first and last is strongly discouraged.'
            );
        }
        
        # paginação padrão
        if (!$filters['first'] && !$filters['last']) {
            $filters['first'] = 100;
        }
        
        if ($filters['after']) {
            $parsedCursor = $this->parseCursor($filters['after']);
            $filters['after'] = $parsedCursor;
        } elseif ($filters['before']) {
            $parsedCursor = $this->parseCursor($filters['before']);
            $filters['before'] = $parsedCursor;
        } elseif (!$filters['id']) {
            $message = $this->repo()->findFirstCleanMessage($filters['peerId']);
            if ($message) {
                $filters['afterIncluding'] = $message->getSentAt();
            }

        }
        
        $nodes = $repo->search($filters);

        $result = [
            'edges' => [],
            'pageInfo' => $repo->getPageInfoData($filters)
        ];
        
        if ($nodes) {
            $result['pageInfo']['startCursor'] = $this->generateCursor(current($nodes)->getSentAt());
            $result['pageInfo']['endCursor'] = $this->generateCursor(end($nodes)->getSentAt());
        }

        foreach ($nodes as $node) {
            $result['edges'][] = [
                'node' => $node,
                'cursor' => $this->generateCursor($node->getSentAt())
            ];
        }
        return $result;
    }
    
    //** todo: needs to be public and static? **/
    public static function generateCursor(\DateTime $sentAt)
    {
        return base64_encode($sentAt->getTimestamp());
    }

    //** todo: needs to be public and static? **/
    public static function parseCursor($cursor)
    {
        $date = new \DateTime('@' . base64_decode($cursor));
        $date->setTimeZone(new \DateTimeZone(date_default_timezone_get()));
        return $date;
    }

    public function clean($data)
    {
        $messageIds = $data['messageIds'];
        $messages = $this->repo()->findByIds($messageIds);


        $threads = [];
        foreach($messages as $message) {
            $thread = $message->getThread();
            if($thread) {
                $threads[] = $thread;
            }
            $message->clean();
        }


        foreach($threads as $t) {
            if($t->getMessages()->count() == 0) {
                $this->em->remove($t);
            }
        }

        $this->em->flush();


        return $messages;
    }

    public function markIrrelevant($data)
    {
        $messageIds = $data['messageIds'];
        $messages = $this->repo()->findByIds($messageIds);
        
        foreach($messages as $message) {
            $message->markAsIrrelevant();
        }
        $this->em->flush();
        
        return $messages;
    }
}