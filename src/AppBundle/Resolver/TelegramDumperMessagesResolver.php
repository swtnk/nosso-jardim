<?php
namespace AppBundle\Resolver;

use Doctrine\ORM\EntityManager;
use League\Flysystem\MountManager;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use AppBundle\Entity\User;
use AppBundle\Entity\Message;
use AppBundle\Entity\Media;
use AppBundle\Entity\MediaType;
use AppBundle\Entity\Peer;

class TelegramDumperMessagesResolver extends EmAwareResolver
{
    protected $mountManager;

    protected $autoFlush = true;
    
    /**
     * cache de usuários para os bulk inserts
     * @var integer[]
     */
    protected static $userCache = [];
    
    /**
     * cache de mensagens para os bulk inserts
     * @var integer[]
     */
    protected static $messageCache = [];

    /**
     * cache de peers para os bulk inserts
     */
    protected static $peerCache = [];

    public function __construct(EntityManager $em, MountManager $mountManager)
    {
        $this->em = $em;
        $this->mountManager = $mountManager;
    }

    public function setAutoFlush($autoFlush)
    {
        $this->autoFlush = $autoFlush;
        return $this;
    }    

    public function getMessageRepository()
    {
        return $this->em->getRepository('AppBundle:Message');
    }

    public function getUserRepository()
    {
        return $this->em->getRepository('AppBundle:User');
    }

    public function getMediaTypeRepository()
    {
        return $this->em->getRepository('AppBundle:MediaType');
    }

    public function loadPeer($json) {
        $repo = $this->em->getRepository('AppBundle:Peer');
        $peerId = $json['to']['peer_id'];
        if( array_key_exists($peerId, self::$peerCache) ) {
            $peer = self::$peerCache[$peerId];
        }
        else {
            $peer = $repo->findByPeerId($peerId);
            
            if(count($peer) == 0) {
                $peer = new Peer;
                $peer->setPeerId($peerId);
            }
            else {
                $peer = $peer[0];
            }
            $peer->setPeerType($json['to']['peer_type']);
            $peer->setPrintName($json['to']['print_name']);
            self::$peerCache[$peerId] = $peer;
        }
        return $peer;
    }


    public function register($data)
    {
        $json = $data['json'];
        if (!is_array($json)) {
            $json = json_decode(stripslashes($json), true);
        }
        
        $telegramMessageId = $json['id'];
        $message = $this->getMessageRepository()->findOneBy(['telegramId' => $telegramMessageId]);
        if ($message) {
            if ($this->hasUserPhoto($json)) {
                $this->mountDumperMediaDirectory($data['media_directory']);
            }
            $this->importPhoto($message->getSender(),$json);
            throw new \InvalidArgumentException('Message already imported.');
        }

        $this
            ->checkMessageAlreadyImported($telegramMessageId)
            ->checkMessageData($json);
        
        if ($this->hasMedia($json) || $this->hasUserPhoto($json)) {
            if(!isset($data['media_directory'])) {
                throw new \InvalidArgumentException("you need to inform a 'media_directory'.");
            }
            $this->mountDumperMediaDirectory($data['media_directory']);
        }

        $peer = $this->loadPeer($json);
        $message = new Message(
            $this->getSender($json), 
            new \DateTime('@' . $json['date']),
            $peer,
            $json['text'] ?? null
        );
        
        $message
            ->setTelegramId($telegramMessageId)
            ->setJson($json);
        
        if ($this->hasMedia($json)) {
            $message->setMedia($this->getMedia($json));
        }

        $this->em->persist($message);
        
        # adiciona mensagem no cache
        static::$messageCache[$telegramMessageId] = $message;
        
        if ($this->autoFlush) {
            $this->em->flush();
            static::clearCache();
        }
        
        return $message;
    }
    
    protected function checkMessageAlreadyImported($telegramMessageId)
    {
        # verifica no cache temporário
        if (isset(static::$messageCache[$telegramMessageId])) {
            throw new \InvalidArgumentException('Message already imported.');
        }
        
        return $this;
    }

    public function checkMessageData(array $json)
    {
        $text = $json['text'] ?? null;
        
        if (!$text && !$this->hasMedia($json)) {
            throw new \InvalidArgumentException('Invalid message.');
        }
        
        return $this;
    }
    
    protected function mountDumperMediaDirectory($mediaDirectory)
    {
        if (empty($mediaDirectory)) {
            throw new \InvalidArgumentException('Unknown media directory.');
        }
        
        $local = new Local($mediaDirectory);
        $filesystem = new Filesystem($local);
        $this->mountManager->mountFilesystem('dumper', $filesystem);
        
        return $this;
    }
    
    protected function getSender(array $json)
    {
        $telegramSenderId  = $json['from']['id'];
        
        $sender = $this->getUserRepository()->findOneBy(['telegramId' => $telegramSenderId]);
        
        if (!$sender) {
            # verifica no cache temporário
            if (isset(static::$userCache[$telegramSenderId])) {
                $sender = static::$userCache[$telegramSenderId];
            } else {
                $name = trim($json['from']['first_name'] . ' ' . $json['from']['last_name']);
                $sender = User::fromTelegram($telegramSenderId, $name);
                # adiciona no cache
                static::$userCache[$telegramSenderId] = $sender;
            }
        }
        
        $sender = $this->importPhoto($sender,$json);
        
        $this->em->persist($sender);
        
        return $sender;
    }

    function importPhoto($sender,$json) {
        if (!$sender->getPhoto() && $this->hasUserPhoto($json)) {
            $media = $this->createMedia($json['from']['userPhoto'], 'photo');
            $sender->setPhoto($media);
            $this->em->persist($media);
        }
        return $sender;
    }
    
    protected function getMedia(array $json)
    {
        $filename = $json['media']['file'] ?? null;
        
        if (empty($filename)) {
            return null;
        }
        
        $media = $this->createMedia($filename, $json['media']['type']);
        
        $this->em->persist($media);
        
        return $media;
    }
    
    protected function createMedia($filename, $type)
    {
        $dumperFs = $this->mountManager->getFilesystem('dumper');
        
        if (!$dumperFs->has($filename)) {
            throw new \InvalidArgumentException('Media file not found.');
        }
        
        $newFilename = $this->getRandomHash();
            
        $size = $dumperFs->getSize($filename);

        $mimetype = $dumperFs->getMimetype($filename);

        $this->mountManager->copy(
            'dumper://' . $filename,
            'storage://' . $newFilename
        );

        $typeMap = [
            'photo' => MediaType::PHOTO,
            'video' => MediaType::VIDEO,
            'audio' => MediaType::AUDIO,
            'document' => MediaType::DOCUMENT
        ];

        
        $mediaType = $this->getMediaTypeRepository()->find($typeMap[$type]);

        if(!$mediaType) {
            throw new \Exception("Media type " . $type . " not found", 1);
        }
        
        return new Media($mediaType, $newFilename, basename($filename), $size, $mimetype);
    }

    protected function getRandomHash()
    {
        return md5(uniqid(rand(), true));
    }
    
     protected function hasMedia(array $json)
    {
        $file = $json['media']['file'] ?? null;
        return !empty($file);
    }
    

    protected function hasUserPhoto(array $json)
    {
        $photo = $json['from']['userPhoto'] ?? null;
        return !empty($photo);
    }
    
    public static function clearCache()
    {
        static::$messageCache = [];
        static::$userCache = [];
        static::$peerCache = [];
    }
}