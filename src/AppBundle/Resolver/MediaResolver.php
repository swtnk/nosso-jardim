<?php

namespace AppBundle\Resolver;


class MediaResolver
{
    private $router;
    
    public function __construct($router) {
        $this->router = $router;
    }    

    public function url($value)
    {
        $route = $this->router->generate(
            'media_download', 
            ['filename' => $value->getFilename()], 
            $this->router::ABSOLUTE_URL
        );
        
        return $route;
    }
}
