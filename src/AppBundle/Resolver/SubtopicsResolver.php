<?php
namespace AppBundle\Resolver;

use Symfony\Component\HttpFoundation\ParameterBag;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Subtopic;

class SubtopicsResolver extends EmAwareResolver
{

    public function repo($entityName = 'AppBundle:Subtopic') {
        return parent::repo($entityName);
    }
    
    public function delete($args)
    {
    	$repo = $this->repo();
    	$subtopic = $repo->find($args['id']);
        $this->em->remove($subtopic);
	    $this->em->flush();
        return $this->repo()->getRootNodes();
    }

    public function registerFirstLevel($args)
    {
        $name = $args['name'];
        $rootNodes = $this->repo()->getRootNodes();
        if(count($rootNodes) == 0) {
            $root = new Subtopic();
            $root->setName('root');
            $this->em->persist($root);
            $this->em->flush();
        }
        else {
            $root = $rootNodes[0];
        }
        $subtopic = new Subtopic;
        $subtopic->setName($name);
        $subtopic->setParent($root);
        $this->em->persist($subtopic);
        $this->em->flush($subtopic);
        $this->em->clear();

        $root = $this->repo()->getRootNodes();
        return $root;
    }

    public function register($args)
    {
        $name = $args['name'];
        $subtopicId = $args['parentId'];
        $parentSubtopic = $this->repo()->find($subtopicId);
        if(!$parentSubtopic) {
            throw new \Exception("Parent subtopic " . $subtopicId . " not found", 1);
        }

        $subtopic = new Subtopic;
        $subtopic->setName($name);
        $subtopic->setParent($parentSubtopic);
        $this->em->persist($subtopic);
        $this->em->flush($subtopic);
        $this->em->clear();
        return $this->repo()->getRootNodes();
    }

    public function findAll($args)
    {
        if($args['root'] == true) {
            return $this->repo()->getRootNodes();
        }
        elseif($args['canHaveItems'] == true) {
            return $this->repo()->findByCanHaveItems(true);
        }

    }

    public function move($args)
    {
        $id = $args['id'];
        $parentId = $args['parentId'];
        $offset = $args['offset'];

        $repo = $this->repo();
        $subtopic = $repo->find($id);

        if($parentId == 0) {
            $newParent = $subtopic->getRoot();
        }
        else {
           $newParent = $repo->find($parentId);
        }
        $subtopic->setParent($newParent);
        $this->em->flush();

        //\Doctrine\Common\Util\Debug::dump();
        $location = $subtopic->getParent()->getChildren()->indexOf($subtopic);
        $distance = $offset - $location;

        if($distance < 0) {
            for($a = 0; $a > $distance; $a--) {
                $repo->moveUp($subtopic);
            }
        }

        if($distance > 0) {
            for($a = 0; $a < $distance; $a++) {
                $repo->moveDown($subtopic);
            }
        }
        
        $this->em->flush();
        $this->em->clear();

        return $this->repo()->getRootNodes();
    }

    public function edit($args)
    {
        $id = $args['id'];
        $repo = $this->repo();
        $subtopic = $repo->find($args['id']);

        if($args['name']) {
           $name = $args['name'];
           $subtopic->setName($name);
        }
        if($args['canHaveItems']) {
            $canHaveItems = $args['canHaveItems'];
            $subtopic->setCanHaveItems($canHaveItems);
        }

        if(!$subtopic) {
            throw new \RuntimeException("subitem $id not found", 1);
        }

        $this->em->flush();
        return $subtopic;
    }

    public function find($args)
    {
        $subtopic = $this->repo()->find($args['id']);
        if(!$subtopic) throw new \RuntimeException("Subtopic not Found", 1);
        return $subtopic;        
    }

    public function path($subtopic) {
        $names = [];
        foreach($subtopic->getAncestors() as $a) {
            $names[] = $a->getName();
        }
        array_shift($names);
        $names[] = $subtopic->getName();
        return implode(' > ',$names);
    }

}