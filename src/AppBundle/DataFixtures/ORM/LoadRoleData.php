<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Role;

class LoadRoleData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new Role(Role::USER, 'User'));
        $manager->persist(new Role(Role::CLASSIFIER, 'Classifier'));
        $manager->persist(new Role(Role::ADMIN, 'Administrator'));
        $manager->flush();
    }
}
