<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\MediaType;

class LoadMediaTypeData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new MediaType(MediaType::PHOTO, 'Photo'));
        $manager->persist(new MediaType(MediaType::DOCUMENT, 'Document'));
        $manager->persist(new MediaType(MediaType::AUDIO, 'Audio'));
        $manager->persist(new MediaType(MediaType::VIDEO, 'Video'));
        $manager->flush();
    }
}
