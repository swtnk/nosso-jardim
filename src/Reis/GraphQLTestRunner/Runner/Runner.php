<?php 
namespace Reis\GraphQLTestRunner\Runner;
use Overblog\GraphQLBundle\Request\ParserInterface;
use Peekmo\JsonPath\JsonStore;

class Runner {
    /**
     * Youshido GraphQL:
     */
    // public function processGraphQL($queryOrArray)
    // {
    //     $variables = null;
    //     if(is_array($queryOrArray)) {
    //         $query = $queryOrArray[0];
    //         $variables = $queryOrArray[1];
    //     }
    //     else {
    //         $query = $queryOrArray;
    //     }
    //     $this->em->clear();
    //     $processor = $this->getService('graphql.processor');
    //     $processor->getExecutionContext()->clearErrors();
    //     $processor->processPayload($query,$variables);
    //     return $processor->getResponseData();
    // }

    public function __construct($executor,$test) {
        $this->executor = $executor;
        $this->test = $test;
    }

    public function processGraphQL($queryOrArray, $throwExceptions = true)
    {
        $executor = $this->executor;

        $variables = null;
        if(is_array($queryOrArray)) {
            $query = $queryOrArray[0];
            $variables = $queryOrArray[1];
            //$operationName = $queryOrArray[2];
        }
        else {
            $query = $queryOrArray;
        }
        $params = [
            ParserInterface::PARAM_QUERY=>$query,
            ParserInterface::PARAM_VARIABLES=>$variables,
            //ParserInterface::PARAM_OPERATION_NAME=>$operationName
        ];
        $schemaName = null;
        $executor->setThrowException($throwExceptions);
        $data = $executor->execute($params, [], $schemaName)->toArray();
        return $data;

        // Exemplo de teste de integração: 
        // https://github.com/mcg-web/graphql-symfony-doctrine-sandbox/blob/master/tests/AppBundle/Functional/WebTestCase.php
    }



    public function gqlData($query, $pathToReturnData = null)
    {

        $result = $this->processGraphQL($query);
        
        if (array_key_exists('errors', $result)) {
            $error = 
                'This query returned errors: ' . 
                print_r($query,1) . 
                "\n\n" . 
                print_r($result, 1);
            throw new \Exception($error);
        }
        else {
            // $this->assertTrue(false, $error);
            if ($pathToReturnData) {
                return $this->getPathValue($pathToReturnData, $result['data']);
            }
            return $result['data']; 
        }

        
        
    }

    private function getPathValue($indexes, $arrayToAccess)
    {
        if (count($indexes) > 1) {
            return $this->getPathValue(array_slice($indexes, 1), $arrayToAccess[$indexes[0]]);
        }
        
        return $arrayToAccess[$indexes[0]];
    }
 
    public function gqlError($query)
    {
        $result = $this->processGraphQL($query, false);

        if (!array_key_exists('errors', $result)) {
            $error = 'This query should, but returned no errors: ' . print_r($query,1) . "\n\n" . print_r($result, 1);
            $this->test->assertTrue(false, $error);
        }
        return $result['errors'];
    }

    public function processResponse($query,$args,$getError = false, $initialPath = '') {
        $jsonPathAcessWrapper = function ($res, $initialPath) {
            return function(...$paths) use ($res, $initialPath) {
                $result = [];
                $runPaths = true;
                if(count($paths)==0) {
                    if($initialPath !== '') {
                        $paths = [''];
                    }
                    else {
                        $result[] = $res;
                        $runPaths = false;
                    }
                }
                if($runPaths) {
                    foreach($paths as $k=>$path) {
                        if($path == '...') continue;
                        $store = new JsonStore();
                        $nextPathIndex = $k+1;
                        if($path !== '') {
                            if($initialPath) {
                                $path = $initialPath . '.' . $path;
                            }
                        }
                        else{
                            if($initialPath) {
                                $path = $initialPath;
                            }
                        }
                        $nextPath = $paths[$nextPathIndex] ?? null;
                        if($nextPath === '...') {
                            $pathResult = $store->get($res,$path);
                        }
                        else {
                            $r = $store->get($res,$path);
                            $numResults = count($r);
                            if($numResults != 1) {
                                throw new \RuntimeException("Search for this path ('$path') returned a result with a count that is not 1 ($numResults): '" . $path . "'. Please notice that if you want an array as a result, you need to use the '...' string as the next argument.", 1);
                            }
                            if($path) {
                                $pathResult = $store->get($res,$path)[0];
                            }
                            else {
                                $pathResult = $res;
                            }
                        }
                        $result[] = $pathResult;
                    }
                }
                if(count($result)==1) {
                    $result = $result[0];
                }
                return $result;

            };
        };

        $queryArray = [$query,$args];
        if($getError) {
            return $this->gqlError($queryArray);
        }
        else {
            $result = $this->gqlData($queryArray);
            return $jsonPathAcessWrapper($result,$initialPath);
        }
    }



}