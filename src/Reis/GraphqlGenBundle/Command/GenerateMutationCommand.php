<?php

namespace Reis\GraphqlGenBundle\Command;
use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Reis\GraphqlGenBundle\Generator\GraphQLMutationGenerator;

class GenerateMutationCommand extends GeneratorCommand
{
    const MAX_ATTEMPTS = 5;

    /**
     * @see Command
     */
    public function configure()
    {
        $this
            ->setName('reis:graphql:generate_mutation')
            ->setDescription('Generates a GraphQL mutation')
            ->setDefinition(array(
                new InputArgument('bundle', InputArgument::OPTIONAL, 'The bundle where the command is generated'),
                new InputArgument('namespace', InputArgument::OPTIONAL, 'The namespace (dir) inside the bundle where the mutation is generated'),
                new InputArgument('name', InputArgument::OPTIONAL, 'The mutations\'s name'),
                new InputArgument('type', InputArgument::OPTIONAL, 'The mutations\'s type. Returned by getType()'),
                new InputArgument('description', InputArgument::OPTIONAL, 'The mutation description (to the docs)')

            ))
            ->setHelp(<<<EOT
The <info>generate:graphql:mutation</info> command helps you generate a new mutations
Provide the bundle name as the first argument, the namespace
as the second argument and the mutation name as the third:

<info>php app/console generate:mutation AppBundle Areas Delete "This mutation deletes an Area"</info>

If any argument is missing, the command will ask for it
interactively. If you want to disable any user interaction, use
<comment>--no-interaction</comment>, but don't forget to pass all needed arguments.

Every generated file is based on a template. 
EOT
            )
        ;
    }

    public function interact(InputInterface $input, OutputInterface $output)
    {
        $bundle = $input->getArgument('bundle');
        $namespace = $input->getArgument('namespace');
        $name = $input->getArgument('name');
        $type = $input->getArgument('type');
        $description = $input->getArgument('description');

        if (
            null !== $bundle &&
            null !== $name &&
            null !== $namespace &&
            null !== $type &&
            null !== $description
        ) {
            return;
        }

        $questionHelper = $this->getQuestionHelper();
        $questionHelper->writeSection($output, 'Welcome to the GraphQL command generator');

        // bundle
        if (null !== $bundle) {
            $output->writeln(sprintf('Bundle name: %s', $bundle));
        } else {
            $output->writeln(array(
                '',
                'First, you need to give the name of the bundle where the command will',
                'be generated (e.g. <comment>AppBundle</comment>)',
                '',
            ));

            $bundleNames = array_keys($this->getContainer()->get('kernel')->getBundles());

            $question = new Question($questionHelper->getQuestion('Bundle name', $bundle), $bundle);
            $question->setAutocompleterValues($bundleNames);
            $question->setValidator(function ($answer) use ($bundleNames) {
                if (!in_array($answer, $bundleNames)) {
                    throw new \RuntimeException(sprintf('Bundle "%s" does not exist.', $answer));
                }

                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $bundle = $questionHelper->ask($input, $output, $question);
            $input->setArgument('bundle', $bundle);
        }

        if (null !== $namespace) {
            $output->writeln(sprintf('Inner Namespace name: %s', $namespace));
        } else {
            $output->writeln(array(
                '',
                'Second, you need to give the name of the namespace where the command will',
                'be generated (e.g. <comment>Subtopics</comment>)',
                '',
            ));

            $srcDir = $this->getContainer()->get('kernel')->getRootDir().'/../src/' . $bundle . '/GraphQL';
            $files = scandir($srcDir);
            $namespaceNames = [];
            foreach($files as $file) {
                if($file == '.' || $file == '..') continue;
                if(is_dir($srcDir.'/'.$file)) {
                    $namespaceNames[] = $file;
                }
            }
            
            $question = new Question($questionHelper->getQuestion('Inner namespace name', $namespace), $namespace);
            $question->setAutocompleterValues($namespaceNames);
            $question->setValidator(function ($answer) use ($namespaceNames) {
                if (empty($answer)) {
                    throw new \RuntimeException(sprintf('Inner namespace cannot be empty'));
                }
                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $namespace = $questionHelper->ask($input, $output, $question);
            $input->setArgument('namespace', $namespace);
        }

        // mutation name
        if (null !== $name) {
            $output->writeln(sprintf('Mutation name: %s', $name));
        } else {
            $output->writeln(array(
                '',
                'Now, provide the name of the mutation as you type it in the console',
                '(e.g. <comment>Delete</comment>)',
                '',
            ));

            $question = new Question($questionHelper->getQuestion('Mutation name', $name), $name);
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                   throw new \RuntimeException('The mutation name cannot be empty.');
                }

                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $name = $questionHelper->ask($input, $output, $question);
            $input->setArgument('name', $name);
        }

        // mutation type
        if (null !== $type) {
            $output->writeln(sprintf('Mutation type: %s', $type));
        } else {
            $output->writeln(array(
                '',
                'Now, provide the type (return) of the mutation as you type it in the console',
                '(e.g. <comment>SubtopicType</comment>)',
                '',
            ));

            $question = new Question($questionHelper->getQuestion('Mutation type', $type), $type);
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                   throw new \RuntimeException('The mutation type cannot be empty.');
                }

                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $type = $questionHelper->ask($input, $output, $question);
            $input->setArgument('type', $type);
        }

        // mutation description
        if (null !== $description) {
            $output->writeln(sprintf('Mutation description: %s', $description));
        } else {
            $output->writeln(array(
                '',
                'Now, provide the description of the mutation as you type it in the console',
                '(e.g. <comment>Deletes an Area</comment>)',
                '',
            ));

            $question = new Question($questionHelper->getQuestion('Mutation description', $description), $description);
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                   throw new \RuntimeException('The mutation description cannot be empty.');
                }

                return $answer;
            });
            $question->setMaxAttempts(self::MAX_ATTEMPTS);

            $description = $questionHelper->ask($input, $output, $question);
            $input->setArgument('description', $description);
        }

        // summary and confirmation
        $output->writeln(array(
            '',
            $this->getHelper('formatter')->formatBlock('Summary before generation', 'bg=blue;fg-white', true),
            '',
            sprintf('You are going to generate a <info>%s</info> mutation inside <info>%s</info> namespace inside <info>%s</info> bundle.', $name, $namespace, $bundle),
        ));

        $question = new Question($questionHelper->getQuestion('Do you confirm generation', 'yes', '?'), true);

        if (!$questionHelper->ask($input, $output, $question)) {
            $output->writeln('<error>Command aborted</error>');
            return 1;
        }
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getQuestionHelper();
        $bundle = $input->getArgument('bundle');
        $namespace = $input->getArgument('namespace');
        $name = $input->getArgument('name');
        $type = $input->getArgument('type');
        $description = $input->getArgument('description');

        try {
            $bundle = $this->getContainer()->get('kernel')->getBundle($bundle);
        } catch (\Exception $e) {
            $output->writeln(sprintf('<bg=red>Bundle "%s" does not exist.</>', $bundle));
        }

        $generator = $this->getGenerator($bundle);
        $generator->generate($bundle, $namespace, $name, $type, $description);

        $output->writeln(sprintf('Generated the <info>%s</info> mutation in <info>%s</info>', $name, $bundle->getName()));
        $questionHelper->writeGeneratorSummary($output, array());
    }

    protected function createGenerator()
    {
        return new GraphQLMutationGenerator($this->getContainer()->get('filesystem'));
    }
}
