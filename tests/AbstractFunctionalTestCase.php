<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractFunctionalTestCase extends WebTestCase
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var DatabaseFileManager
     */
    private $dbFileManager;

    protected function setUp()
    {
        parent::setUp();

        self::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->em = $this->container->get('doctrine.orm.entity_manager');

        $databaseFile = $this->em->getConnection()->getDatabase();

        $this->dbFileManager = new DatabaseFileManager(
            $databaseFile,
            $databaseFile . '.bak',
            new Application(static::$kernel)
        );

        $this->dbFileManager->createDatabase();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
        $this->em = null;
        $this->dbFileManager->restoreDatabase();
    }
}