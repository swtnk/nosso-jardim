<?php
namespace Tests\AppBundle\GraphQL\Items;
use Tests\AppBundle\GraphQL\TestHelper;

class ItemTestHelper extends TestHelper{

    function ITEMS_REGISTER($args = [],$getError = false, $initialPath = 'itemsRegister') {
        $q ='mutation itemsRegister(
                $name:String!
                $subtopicId:ID!
            ){
                    itemsRegister(
                        name:$name
                        subtopicId:$subtopicId
                    ){
                        id
                    }
                }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function ITEMS_EDIT($args = [],$getError = false, $initialPath = 'itemsEdit') {
        $q ='mutation itemsEdit(
                $id: ID!
                $name: String!
            ){
                itemsEdit(
                    id: $id
                    name: $name
                ){
                    id
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function ITEMS_DELETE($args = [],$getError = false, $initialPath = 'itemsDelete') {
        $q ='mutation itemsDelete(
                $id: ID!
            ){
                    itemsDelete(
                        id: $id
                    ){
                        id
                    }
                }
            ';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);    
    }

    function ITEMS_QUERY($args = [],$getError = false, $initialPath = 'items') {
        $q ='query items($subtopicId:ID){
            items(subtopicId:$subtopicId){
                id
                name
            }
        }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);    
    }

    function ITEM_QUERY($args = [],$getError = false, $initialPath = '') {
        $q ='query item($id:ID!){
                item(id:$id){
                    id
                    name
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    public function addItem($name,$subtopicId)
    {
        return $this->ITEMS_REGISTER([
            'name'=>$name,
            'subtopicId'=>$subtopicId
        ])('id');
    }

}