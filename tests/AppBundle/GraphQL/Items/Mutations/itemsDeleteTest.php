<?php
namespace Tests\AppBundle\GraphQL\Items\Mutations;
use Tests\AppBundle\GraphQL\Items\ItemTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Entity\Item;
use AppBundle\Entity\Area;


class ItemsDeleteTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new ItemTestHelper($this);
    }

    /**
     * @test
     */
    public function shouldCreateTwoDeleteOneAndCount()
    {
        $h = $this->helper();

        $subtopic1Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Planta"])('0.subtopics.0.id');
        $subtopic2Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Cultivo"])('0.subtopics.1.id');
        $h->SUBTOPICS_EDIT(['id'=>$subtopic1Id,'canHaveItems'=>true]);
        $h->SUBTOPICS_EDIT(['id'=>$subtopic2Id,'canHaveItems'=>true]);

        $item1Id = $h->addItem("Mogno", $subtopic1Id);
        $item2Id = $h->addItem("Alface", $subtopic2Id);

        $this->assertCount(
            2,
            $h->ITEMS_QUERY()()
        );

        $h->ITEMS_DELETE(['id'=>$item1Id]);

        $this->assertCount(
            1,
            $h->ITEMS_QUERY()()
        );
    }
}