<?php
namespace Tests\AppBundle\GraphQL\Items\Queries;
use Tests\AppBundle\GraphQL\Items\ItemTestHelper;
use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;

class ItemsTest extends AbstractIntegrationTestCase
{
    
    function helper() {
        return new ItemTestHelper($this);
    }
    
    /** @test */
    public function shouldSearchItemsBySubtopic()
    {
        $h = $this->helper();

        $subtopic1Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Planta"])('0.subtopics.0.id');
        $subtopic2Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Construcao"])('0.subtopics.1.id');
        $h->SUBTOPICS_EDIT(['id'=>$subtopic1Id,'canHaveItems'=>true]);
        $h->SUBTOPICS_EDIT(['id'=>$subtopic2Id,'canHaveItems'=>true]);
        
        $h->addItem("Mogno", $subtopic1Id);
        $h->addItem("Alface", $subtopic1Id);
        $h->addItem("Adobe", $subtopic2Id);
        $h->addItem("Cob", $subtopic2Id);

        $names = $h->ITEMS_QUERY([
            'subtopicId'=>$subtopic1Id
        ])('items.*.name','...');

        $this->assertCount(0,array_diff($names,['Mogno','Alface']));
    }
    
       
}
