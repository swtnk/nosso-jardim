<?php
namespace Tests\AppBundle\GraphQL\Items\Queries;

use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Tests\AppBundle\GraphQL\Items\ItemTestHelper;

class ItemTest extends AbstractIntegrationTestCase
{

	function helper() {
        return new ItemTestHelper($this);
    }

    /** @test */
    public function shouldQueryById() {
        $h = $this->helper();

        $subtopic1Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Planta"])('0.subtopics.0.id');
        $subtopic2Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Construcao"])('0.subtopics.1.id');
        $h->SUBTOPICS_EDIT(['id'=>$subtopic1Id,'canHaveItems'=>true]);
        $h->SUBTOPICS_EDIT(['id'=>$subtopic2Id,'canHaveItems'=>true]);

        $iId = $h->addItem("Mogno", $subtopic1Id);
        $h->addItem("Alface", $subtopic1Id);
        $h->addItem("Adobe", $subtopic2Id);

        $item = $h->ITEM_QUERY(['id'=>$iId])('item');
        
        $this->assertEquals('Mogno',$item['name']);
    }
}
