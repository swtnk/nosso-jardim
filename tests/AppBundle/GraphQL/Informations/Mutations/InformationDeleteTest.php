<?php
namespace Tests\AppBundle\GraphQL\Informations\Mutations;
use Tests\AppBundle\GraphQL\Informations\InformationTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
// use AppBundle\Entity\Item;
// use AppBundle\Entity\Area;


class InformationDeleteTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new InformationTestHelper($this);
    }

    /**
     * @test
     */
    public function shouldRemoveInformation()
    {
        $h = $this->helper();

        $s1 = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Planta"])('0.subtopics.0.id');
        $s2 = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Construcao"])('0.subtopics.1.id');
        
        $t1 = $h->createThread();

        $h->INFORMATION_REGISTER_FOR_THREAD([
                'threadId'=>$t1,
                'information'=>['subtopicId'=>$s1]
        ]);

        $i2id = $h->INFORMATION_REGISTER_FOR_THREAD([
                'threadId'=>$t1,
                'information'=>['subtopicId'=>$s2]
        ])('informations.1.id');

        $h->INFORMATION_REMOVE_MUTATION([
            'id'=>$i2id
        ]);

        $informations = $h->THREAD(['id'=>$t1])('thread.informations');
        $this->assertCount(1,$informations);
        $this->assertEquals($s1,$informations[0]['subtopic']['id']);
    }

}
