<?php
namespace Tests\AppBundle\GraphQL\Informations;
use Tests\AppBundle\GraphQL\TestHelper;

class InformationTestHelper extends TestHelper{


    function INFORMATION_REGISTER_FOR_THREAD($args = [],$getError = false, $initialPath = 'informationRegisterForThread') {
        $q = 'mutation informationRegisterForThread(
            	$threadId:ID!
            	$information:InformationInput
            ){
                informationRegisterForThread(
                    threadId:$threadId
                    information:$information
                ){
                    thread {
                        id
                        informations{
                            id
                            subtopic{
                                id
                            }
                        }
                    }
                }
	        }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function INFORMATION_REMOVE_MUTATION($args = [],$getError = false, $initialPath = 'informationRemove') {
        $q ='mutation informationRemove(
            	$id:ID!
            ){
                informationRemove(
                    id:$id
                ){
                    id
                    informations{
                        id
                    }
                }
	        }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

}