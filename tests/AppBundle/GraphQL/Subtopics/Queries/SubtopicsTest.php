<?php
namespace Tests\AppBundle\GraphQL\Subtopics\Queries;

use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Tests\AppBundle\GraphQL\Subtopics\SubtopicsTestHelper;

class SubtopicsTest extends AbstractIntegrationTestCase
{

	function helper() {
        return new SubtopicsTestHelper($this);
    }
    
    /** @test */
    public function shouldQueryRootNode()
    {
    	$h = $this->helper();
    	$h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>'just to create root']);
        $root = $h->SUBTOPICS_QUERY(['root'=>true])('0');
        $this->assertEquals('root',$root['name']);
    }    

    /** @test */
    public function shouldReturnNodesThatCanHaveItems() {
    	$h = $this->helper();

        $nIds = [];
        foreach(['n1','n2','n3'] as $k=>$n) {
            $nIds[] = $h->SUBTOPICS_REGISTER_FIRST_LEVEL([
                'name'=>$n
            ])(
                '0.subtopics.'.$k.'.id'
            );
        }

    	$n1childId = $h->SUBTOPICS_REGISTER([
            'parentId' => $nIds[0],
            'name'     => 'n1child'
        ])('0.subtopics.0.subtopics.0.id');

        $h->SUBTOPICS_EDIT([ 'id'=>$nIds[1] ,   'canHaveItems'=>true]);
    	$h->SUBTOPICS_EDIT([ 'id'=>$n1childId , 'canHaveItems'=>true]);


        list(
            $subtopicIds,
            $n1ChildAncestors
        ) = $h->SUBTOPICS_QUERY([
                'canHaveItems'=>true
        ],null,null)(
            'subtopics.*.id','...',
            sprintf(
                'subtopics[?(@.id==%d)].ancestors',
                $n1childId
            )
        );

        $this->assertContains($nIds[1],$subtopicIds);
        $this->assertContains($n1childId,$subtopicIds);

        $this->assertEquals('root',$n1ChildAncestors[0]['name']);
        $this->assertEquals('n1',$n1ChildAncestors[1]['name']);
    }

    
}
