<?php
namespace Tests\AppBundle\GraphQL\Subtopics\Queries;

use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Tests\AppBundle\GraphQL\Subtopics\SubtopicsTestHelper;

class SubtopicTest extends AbstractIntegrationTestCase
{

	function helper() {
        return new SubtopicsTestHelper($this);
    }

    /** @test */
    public function shouldQueryById() {
        $h = $this->helper();

        $n = $h->SUBTOPICS_REGISTER_FIRST_LEVEL([
            'name'=>'n1'
        ]);

        $n1Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL([
            'name'=>'n1'
        ])('0.subtopics.0.id');

        $name = $h->SUBTOPIC(['id' => $n1Id])('subtopic.name');
        $this->assertEquals( 'n1' , $name );
    }

    /** @test */
    public function shouldThrowErrorIfSubtopicDoesNotExists() {
        $h = $this->helper();
        $nonExistent = 999;
        $name = $h->SUBTOPIC([
            'id' => $nonExistent
        ],true);
    }
    
}
