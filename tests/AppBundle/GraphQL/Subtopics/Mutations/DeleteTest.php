<?php
namespace Tests\AppBundle\GraphQL\Subtopics\Mutations;

use Tests\AppBundle\GraphQL\Subtopics\SubtopicsTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;


class DeleteTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new SubtopicsTestHelper($this);
    }
    
    /** @test */
    public function shouldDeleteASubtopic()
    {
        $h = $this->helper();

        $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>'sub1']);
        $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>'sub2']);

        list(
            $subtopics,
            $subId_1
        ) = $h->SUBTOPICS_QUERY(
            ['root'=>true]
        )(
            '0.subtopics',
            '0.subtopics.0.id'
        );

        $this->assertCount(2,$subtopics);
   	
        $ret = $h->SUBTOPIC_DELETE(['id'=>$subId_1]);

        list(
            $subtopics,
            $name
        ) = $h->SUBTOPICS_QUERY(['root'=>true])(
            '0.subtopics',
            '0.subtopics.0.name'
        );
        $this->assertCount(1,$subtopics);
        $this->assertEquals('sub2',$name);
    }

}