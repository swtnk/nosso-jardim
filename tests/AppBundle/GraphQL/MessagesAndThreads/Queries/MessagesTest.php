<?php
use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class MessagesTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }

    private function registerMessages($quantity)
    {
        $h = $this->helper();
        $ids = [];
        
        $time = time();
        
        for ($i = 0; $i < $quantity; $i++) {
            $ids[] = $h->registerTelegramDumperMessage(
                [
                    'text' => $this->getFaker()->text,
                    'date' => $time + $i
                ],
                ['registerTelegramDumperMessage', 'id']
            );
        }
        
        return $ids;
    }
    
    /**
     * @test
     */
    public function shouldSearchFirstTwoMessages()
    {
        $h = $this->helper();
        $ids = $this->registerMessages(3);
        $variables = ['first'=>2];
        $result = $h->MESSAGES($variables)();
        $this->assertCount(2, $result['edges']);
        $this->assertEquals($ids[0], $result['edges'][0]['node']['id']);
        $this->assertEquals($ids[1], $result['edges'][1]['node']['id']);
        $this->assertFalse($result['pageInfo']['hasPreviousPage']);
    }
    
    /**
     * @test
     */
    public function shouldSearchFirstMessageAfterSecondMessage()
    {
        $h = $this->helper();
        $ids = $this->registerMessages(4);
       
        $messages = $h->MESSAGES()();
        $this->assertCount(4, $messages['edges']);
        $afterCursor = $messages['edges'][1]['cursor'];
        
        $messages = $h->MESSAGES([
            'after'=>$afterCursor,
            'first'=>1
        ])();
        
        $this->assertCount(1, $messages['edges']);
        $this->assertEquals($ids[2], $messages['edges'][0]['node']['id']);
        $this->assertTrue($messages['pageInfo']['hasPreviousPage']);
        $this->assertTrue($messages['pageInfo']['hasNextPage']);
    }
    
    /**
     * @test
     */
    public function shouldSearchLastTwoMessagesBeforeFourthMessage()
    {
        $h = $this->helper();
        $ids = $this->registerMessages(5);
        
        $beforeCursor = $h->MESSAGES()('edges.3.cursor');
        
        list($edges,$pageInfo) = $h->MESSAGES([
            'before'=>$beforeCursor, 
            'last'=>2
        ])('edges','pageInfo');
        $this->assertCount(2, $edges);
        $this->assertEquals($ids[1], $edges[0]['node']['id']);
        $this->assertEquals($ids[2], $edges[1]['node']['id']);
        $this->assertTrue($pageInfo['hasPreviousPage']);
        $this->assertTrue($pageInfo['hasNextPage']);
    }
    
    /**
     * @test
     */
    public function shouldReturnCursors()
    {
        $h = $this->helper();
        $ids = $this->registerMessages(3);
        $messages = $h->MESSAGES( ['first'=>2] )();
        $this->assertCount(2, $messages['edges']);
        $this->assertArrayHasKey('startCursor', $messages['pageInfo']);
        $this->assertArrayHasKey('endCursor', $messages['pageInfo']);
        $this->assertFalse($messages['pageInfo']['hasPreviousPage']);
        $this->assertTrue($messages['pageInfo']['hasNextPage']);
    }
    
    /**
     * @test
     */
    public function shouldSearchMessagesWithIdFilter()
    {
        $h = $this->helper();
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text' => 'Ola, tudo bem?'], 
            ['registerTelegramDumperMessage', 'id']
        );

        

        $edges = $h->MESSAGES([
            'id'=>$messageId1
        ])('edges');

        $this->assertCount(
            1,
            $edges,
            'deve inserir uma mensagem'
        );

        $this->assertEquals(
            $messageId1,
            $edges[0]['node']['id'],
            'o id consultado deve ser o mesmo retornado na criação'
        );

        $this->assertEquals(
            'Ola, tudo bem?',
            $edges[0]['node']['text'],
            'o texto consultado deve ser o mesmo enviado na criação'
        );
    }

    /**
     * @test
     */
    public function shouldStartOnFirstNonIrrelevantAndNonThreadedMessage()
    {
        $h = $this->helper();
        $ids = $this->registerMessages(7);

        $h->MESSAGES_MARK_IRRELEVANT([
            'messageIds'=> [ $ids[0] ]
        ]);

        $id = $h->MESSAGES([
            'first'=>9
        ])('edges.0.node.id');
        
        $this->assertEquals( $ids[1] , $id );

        $h->THREAD_REGISTER([
            'messageIds'=>[ $ids[1] , $ids[3] ]
        ]);

        $id = $h->MESSAGES([
            'first'=>9
        ])('edges.0.node.id');

        $this->assertEquals( $ids[2], $id );
    }

    /**
     * @test
     */
    public function shouldSearchLastMessageBeforeSecondMessage()
    {
        $ids = $this->registerMessages(7);
        $h = $this->helper();

        $h->MESSAGES_MARK_IRRELEVANT([
            'messageIds'=> [ $ids[0], $ids[1], $ids[2] ]
        ]);

        $messages = $h->MESSAGES()();

        $this->assertEquals( $ids[3] , $messages['edges'][0]['node']['id'] );
        $cursor = $messages['edges'][0]['cursor'];

        $messages = $h->MESSAGES([
            'last'   => 2,
            'before' => $cursor
        ])();

        $this->assertEquals(
            $ids[1],
            $messages['edges'][0]['node']['id'],
            "The first of last two should be the second inserted:".$ids[1]);
        $this->assertEquals(
            $ids[1],
            $messages['edges'][0]['node']['id'],
            "The second of last two should be the third inserted:".$ids[2]);
    }
    
    /**
     * @test
     */
    public function shouldMediaMessageContainsMedia()
    {
        $fixture = new LoadMediaTypeData();
        $fixture->load($this->em);        
        
        $mediaDir = realpath(__DIR__ . '/../../../../data');
        $h = $this->helper();
        $id = $h->registerTelegramDumperMessage(
            [
                'media' => [
                    'type' => 'photo',
                    'caption' => 'imagem de teste',
                    'file' => 'image.jpg'
                ]
            ],
            ['registerTelegramDumperMessage', 'id'],
            null,
            $mediaDir
        );
        
        $media = $h->MESSAGES([
            'id'=>$id
        ])('edges.0.node.media');

        $this->assertEquals( 'image.jpg' , $media['filename'] );
        $this->assertNotEmpty($media['url']);
    }

    /**
     * @test
     */
    public function shouldFilterByPeer()
    { 

        $h = $this->helper();
        $ids = $this->registerMessages(3);
        $h->registerTelegramDumperMessage(
            [
                'text' => $this->getFaker()->text, 
                'date' => time() + rand(1,3000)
            ],
            ['registerTelegramDumperMessage', 'id'],
            'SEMENTES'
        );

        $peers = $h->PEERS()();

        $phpDfId = $peers[0]['id'];
        $sementesId = $peers[1]['id'];

        $edges = $h->MESSAGES([
            'peerId'=>$phpDfId
        ])('edges');
        $this->assertCount(3,$edges);
        
        $edges = $h->MESSAGES([
            'peerId'=>$sementesId
        ])('edges');
        $this->assertCount(1,$edges);
    }
}
