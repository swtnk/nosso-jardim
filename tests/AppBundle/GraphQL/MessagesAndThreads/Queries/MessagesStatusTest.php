<?php
use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class MessagesStatusTest extends AbstractIntegrationTestCase
{

    public static $sementesPeerId = 1;
    public static $phpdfPeerId = 2;

    function messagesStatusQuery($fields) {
        return function($peerId) use ($fields) {
            return [
                sprintf(
                    'query messagesStatus($peerId:String){
                        messagesStatus(peerId:$peerId) %s
                    }',
                    $fields
                ),
                ['peerId'=>$peerId] // variables
            ];
        };
    }

    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }

    private function registerMessage(
        $peer,
        $date,
        $irrelevant,
        $addToThread
    )
    {
        $h = $this->helper();
        $id = $h->registerTelegramDumperMessage(
            [
                'text' => $this->getFaker()->text, 
                'date' => DateTime::createFromFormat('d/m/Y H:i', $date.' 00:00')->getTimestamp()
            ],
            ['registerTelegramDumperMessage', 'id'],
            $peer
        );

        if($irrelevant) {
            $this->gql("mutation{
                                messagesMarkIrrelevant(messageIds:[".$id."]){id}
                            }");
        }

        if($addToThread) {
            $this->gql("mutation{
                                threadRegister(messageIds:[".$id."]){id}
                            }");
        }
        return $id;
    }

    protected function setUp()
    {
        parent::setUp();
        $fixture = new LoadMediaTypeData();
        $fixture->load($this->em);
        $this->registerMessage('SEMENTES','10/10/2005',false,true);
        $this->registerMessage('SEMENTES','12/10/2005',true,false);
        $id1 = $this->registerMessage('SEMENTES','9/11/2005',false,false);
        $id2 = $this->registerMessage('SEMENTES','10/11/2005',false,false);
        $this->gql("mutation{
                            threadRegister(messageIds:[".$id1.",".$id2."]){id}
                        }");
        $this->registerMessage('SEMENTES','10/11/2005',false,false);

        $this->registerMessage('PHP_DF','9/9/2003',false,false);
        $this->registerMessage('PHP_DF','10/9/2003',false,false);
        $id1 = $this->registerMessage('PHP_DF','9/10/2003',false,false);
        $id2 = $this->registerMessage('PHP_DF','9/11/2003',false,false);
        $this->gql("mutation{
                            threadRegister(messageIds:[".$id1.",".$id2."]){id}
                        }");

    }

    /**
     * @test
     */
    public function shouldCountMessagesPerPeer()
    {
        $query = $this->messagesStatusQuery('{messagesCount}');
        $r1 = $this->gql($query(self::$sementesPeerId))['messagesStatus']['messagesCount'];
        
        $this->assertEquals(5,$r1);
        $r2 = $this->gql($query(self::$phpdfPeerId))['messagesStatus']['messagesCount'];

        $this->assertEquals(4,$r2);
    }

    /**
     * @test
     */
    public function shouldCountCleanMessagesPerPeer()
    {
        $query = $this->messagesStatusQuery('{
            cleanMessagesCount
        }');

        $r1 = $this->gql($query(self::$sementesPeerId))['messagesStatus']['cleanMessagesCount'];
        $this->assertEquals(1,$r1);

        $r2 = $this->gql($query(self::$phpdfPeerId))['messagesStatus']['cleanMessagesCount'];
        $this->assertEquals(2,$r2);
    }

    /**
     * @test
     */
    public function shouldGetNewestAndOldestDatesPerPeer()
    {
        $query = $this->messagesStatusQuery('{
            newestMessageDate
            oldestMessageDate
        }');

        $r1 = $this->gql($query(self::$sementesPeerId))['messagesStatus'];
        
        $this->assertEquals('2005-11-10 00:00:00',$r1['newestMessageDate']);
        $this->assertEquals('2005-10-10 00:00:00',$r1['oldestMessageDate']);

        $r2 = $this->gql($query(self::$phpdfPeerId))['messagesStatus'];

        $this->assertEquals('2003-11-09 00:00:00',$r2['newestMessageDate']);
        $this->assertEquals('2003-09-09 00:00:00',$r2['oldestMessageDate']);
    }

    /**
     * @test
     */
    public function shouldGetNewestAndOldestThreadDates()
    {
        $query = $this->messagesStatusQuery('{
            newestThreadDate
            oldestThreadDate
        }');


        $r1 = $this->gql($query(self::$sementesPeerId))['messagesStatus'];
        
        $this->assertEquals('2005-10-10 00:00:00',$r1['oldestThreadDate']);
        $this->assertEquals('2005-11-09 00:00:00',$r1['newestThreadDate']);

        $r2 = $this->gql($query(self::$phpdfPeerId))['messagesStatus'];

        $this->assertEquals('2003-10-09 00:00:00',$r2['newestThreadDate']);
        $this->assertEquals('2003-10-09 00:00:00',$r2['oldestThreadDate']);
    }

}
