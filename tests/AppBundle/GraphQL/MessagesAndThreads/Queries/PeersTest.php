<?php

use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class PeersTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->resolver = $this->getService('app.resolver.telegramDumperMessages');
        $fixture = new LoadMediaTypeData();
        $fixture->load($this->em);
    }

    public function registerWithPeer($id,$printName)
    {
        $bag = [
            'event'=> 'message',
            'json' => [
                'id' => rand(1,10000),
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>[
                    'id'=>'$05000000a4122f3fa12d96f5ee4f4305',
                    'peer_type'=>'channel',
                    'peer_id'=>$id,
                    'print_name'=>$printName,
                    'title'=>'PHP DF'
                ],
                'text' => 'teste',
                'date' => time(),
            ]
        ];
        $message = $this->resolver->register($bag);
    }

    
    /**
     * @test
     */
    public function shouldReturnAllPeersSortedByName()
    {
        $h = $this->helper();
        $this->registerWithPeer('456','PHP DF');
        $this->registerWithPeer('123','SEMENTES');
        $this->registerWithPeer('789','ALCUINO');
        
        $peers = $h->PEERS()();
        

        
        $this->assertCount(3, $peers);
        $this->assertEquals('ALCUINO',  $peers[0]['printName']);
        $this->assertEquals('PHP DF',   $peers[1]['printName']);
        $this->assertEquals('SEMENTES', $peers[2]['printName']);
    }
}
