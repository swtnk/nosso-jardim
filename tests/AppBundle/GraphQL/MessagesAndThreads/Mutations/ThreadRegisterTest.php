<?php
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Resolver\Mutation\RegisterTelegramDumperMessageResolver;
use AppBundle\Entity\Message;
use AppBundle\Entity\Thread;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class ThreadRegisterTest extends AbstractIntegrationTestCase
{
    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }
    
    /**
     * @test
     */
    public function shouldRegisterAThreadWithOneMessage()
    {
        $h = $this->helper();
        $messageId1 = $h->registerTelegramDumperMessage(['text'=>'Olá, tudo bem?'],['registerTelegramDumperMessage','id']);
        $messageId2 = $h->registerTelegramDumperMessage(['text'=>'Tudo'],['registerTelegramDumperMessage','id']);
        $messageId3 = $h->registerTelegramDumperMessage(['text'=>'All righ'],['registerTelegramDumperMessage','id']);

        $threadId = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1]
        ])('id');


        $messages = $h->THREAD([
            'id'=>$threadId
        ])('thread.messages');

        
        $this->assertCount(
            1, 
            $messages,
            'a thread inserida deve ter um array com uma mensagem'
        );

        $this->assertEquals(
            $messageId1,
            $messages[0]['id'],
            'a mensagem da thread deve ser a que foi passada para o método'
        );

    }

    /**
     * @test
     */
    public function shouldReturnAnErrorIfAMessageDoesNotExists()
    {
        $h = $this->helper();
        $messageId1 = $h->registerTelegramDumperMessage(['text'=>'Tudo.'],['registerTelegramDumperMessage','id']);
        
        $error = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1,"999"]
        ],true);
    }

    /**
     * @test
     */
    public function shouldCreateAThreadWithTwoMessages()
    {
        $time = time();
        $h = $this->helper();

        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );
        
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );

        $threadId = $h->THREAD_REGISTER([
                'messageIds'=>[$messageId1,$messageId2]
        ])('id');

        $ids = $h->THREAD([
            'id'=>$threadId
        ])('thread.messages.*.id','...');
        
        $this->assertEquals(
            [$messageId1,$messageId2],
            $ids,
            'A thread inserida deve ter as duas mensagens relacionadas'
        );
    }
}
