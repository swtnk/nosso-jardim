<?php
namespace Tests\AppBundle\GraphQL\MessagesAndThreads;
use AppBundle\Entity\Subtopic;
use Tests\AppBundle\GraphQL\TestHelper;

class MessagesAndThreadsTestHelper extends TestHelper{    

    function MESSAGES($args = [],$getError = false, $initialPath = 'messages') {
        $q ='query messages(
                $first:Int
                $last:Int
                $after:String
                $before:String
                $id:ID
                $peerId:ID
            ) {
                messages(
                    first:$first
                    last:$last
                    after:$after
                    before:$before
                    id:$id
                    peerId:$peerId
                ) {
                    edges {
                        node {
                            id
                            sentAt
                            text
                            irrelevant 
                            thread{
                                id
                            }
                            media{
                                id
                                filename
                                url
                                type{
                                    id,
                                    name
                                }
                                size
                                mimetype
                                createdAt
                            }
                        }
                        cursor
                    }
                    pageInfo {
                        hasNextPage
                        hasPreviousPage
                        startCursor
                        endCursor
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }



	function THREAD_REGISTER($args = [],$getError = false, $initialPath = 'threadRegister') {
        $q ='mutation threadRegister(
                $messageIds:[ID!]
            ){
                threadRegister(
                    messageIds:$messageIds
                ){
                    id
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function PEERS($args = [],$getError = false, $initialPath = 'peers') {
        $q = 'query{
                    peers{
                        id
                        printName
                        peerId
                    }
                }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }


    function THREAD_ADD_MESSAGES($args = [],$getError = false, $initialPath = 'threadAddMessages') {
        $q ='mutation threadAddMessages(
                $threadId:ID!
                $messageIds:[ID!]
            ){
                threadAddMessages(
                    messageIds:$messageIds
                    threadId:$threadId
                ){
                    id
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function MESSAGES_CLEAN($args = [],$getError = false, $initialPath = 'messagesClean') {
        $q ='mutation messagesClean(
                $messageIds:[ID!]
            ){
                messagesClean(
                    messageIds:$messageIds
                ){
                    id
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }


    function THREADS($args = [],$getError = false, $initialPath = 'threads') {
        $q ='query threads(
                $id:ID
                $peerId:ID
            ){
                threads(
                    id:$id
                    peerId:$peerId
                ){
                    id
                    messages{
                        id
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function MESSAGES_MARK_IRRELEVANT($args = [],$getError = false, $initialPath = 'messagesMarkIrrelevant') {
        $q ='mutation messagesMarkIrrelevant(
                $messageIds:[ID!]
            ){
                messagesMarkIrrelevant(
                    messageIds:$messageIds
                ){
                    id
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

}