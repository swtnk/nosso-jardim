<?php
namespace Tests\AppBundle\GraphQL;
use AppBundle\Entity\Subtopic;


class TestHelper {

	protected $test;

	function __construct($test) {
        $this->runner = $test->runner;
        $this->em = $test->em();
	}

    public function initRoot() {
        $subtopic = new Subtopic();
        $subtopic->setName('root');
        $this->em->persist($subtopic);
        $this->em->flush();
    }

    public function registerTelegramDumperMessage(
        $msgData, 
        $returnPath = null, 
        $peer = 'PHP_DF', 
        $mediaDir = null
    ) {

        if($peer==null) {
            $peer = 'PHP_DF';        
        }

        if($peer == 'PHP_DF') {
            $to = [
                'id'=>'$05000000a4122f3fa12d96f5ee4f4305',
                'peer_type'=>'channel',
                'peer_id'=>1060049572,
                'print_name'=>'PHP_DF',
                'title'=>'PHP DF'
            ];
        }

        if($peer == 'SEMENTES') {
            $to = [
                "id"=>"$050000002bda8c3f06daaf068f603db6",
                "peer_type"=>"channel",
                "peer_id"=>1066195499,
                "print_name"=>"SEMENTES",
                "title"=>"SEMENTES"
            ];
        }

        $msg = [
            'id' => 'qwerty' . rand(0, 2000),
            'from' => [
                'id' => 1,
                'first_name' => 'Fulano',
                'last_name' => 'da Silva'
            ],
            'to'=>$to,
            'text' => 'teste',
            'date' => time()
        ];
            
        foreach ($msgData as $key => $value) {
            $msg[$key] = $value;
        }

        $jsString = addslashes(json_encode($msg));
        
        $variables = ['json'=>$jsString];

        if($mediaDir) {
            $variables['media_directory'] = $mediaDir;
        }

        $query = '
            mutation registerTelegramDumperMessage(
                $json:String!,
                $media_directory: String
            ){
                registerTelegramDumperMessage(
                    json:$json,
                    media_directory:$media_directory
                ){id}
            }';

        return $this->runner->gqlData([$query,$variables], $returnPath);
    }
    
    function createThread() {
        $time = time() - rand(1,5000);
        $messageIds = [];

        for($a = 0;$a<3;$a++) {
            $messageIds[] = $this->registerTelegramDumperMessage(
                [
                    'text'=>'xxx', 
                    'date' => ++$time
                ],[
                    'registerTelegramDumperMessage',
                    'id'
                ]
            );
        }

        $variables = [
            'messageIds'=>$messageIds
        ];

        $query = '
            mutation threadRegister($messageIds:[ID!]){
                threadRegister(messageIds:$messageIds) {
              id
            }}
        ';
        
        return $this->runner->gqlData(
            [$query,$variables],
            ['threadRegister','id']
        );
    }

    function SUBTOPICS_REGISTER_FIRST_LEVEL($args = [],$getError = false, $initialPath = 'subtopicsRegisterFirstLevel') {
        $q ='mutation subtopicsRegisterFirstLevel(
                $name:String
            ){
                subtopicsRegisterFirstLevel(
                    name:$name
                ){
                    id,
                    name,
                    subtopics{
                        id,
                        name
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    

    public function SUBTOPIC($args = [],$getError = false, $initialPath = '') {
        $q ='query subtopic(
                    $id:ID!
                ){ 
                    subtopic(
                        id:$id
                    ) {
                        id
                        name
                    }
                }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    public function SUBTOPICS_EDIT($args = [],$getError = false, $initialPath = '') {
        $q ='mutation (
                $id:ID!
                $name:String
                $canHaveItems:Boolean
            ){ 
                subtopicsEdit(
                    id:$id
                    name:$name
                    canHaveItems:$canHaveItems
                ) {
                    id
                    name
                    canHaveItems
                    ancestors
                    {
                        id
                        name
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function THREAD($args = [],$getError = false, $initialPath = '') {
        $q ='query thread($id:ID!){
                thread(id:$id){
                    id
                    messages{
                        id
                        text
                    }
                    informations{
                        id
                        about
                        subtopic{
                            id
                        }
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

}
