<?php

use Tests\AbstractIntegrationTestCase;
use Mockery as M;
use AppBundle\Service\TelegramConnector;
use AppBundle\Entity\User;

class ConnectServiceTest extends AbstractIntegrationTestCase
{
    /**
     * @var AppBundle\Service\ConnectService
     */
    protected $connectService;

    protected function setUp()
    {
        parent::setUp();

        $connectorMock = M::mock(TelegramConnector::class);
        $connectorMock->shouldReceive('sendMessage');

        $this->connectService = $this->getService('app.service.connect');
        $this->connectService->setTelegramConnector($connectorMock);
    }

    /**
     * @return array
     */
    protected function getPayload()
    {
        return [
            'update_id' => 859511290,
            'message' =>
            [
                'message_id' => 76,
                'from' =>
                [
                    'id' => 209820072,
                    'first_name' => 'Bruno',
                    'last_name' => 'Neves',
                    'username' => 'brunodasneves',
                ],
                'chat' =>
                [
                    'id' => 209820072,
                    'first_name' => 'Bruno',
                    'last_name' => 'Neves',
                    'username' => 'brunodasneves',
                    'type' => 'private',
                ],
                'date' => 1485825483,
                'text' => '/connect',
                'entities' =>
                [
                    0 =>
                    [
                        'type' => 'bot_command',
                        'offset' => 0,
                        'length' => 8,
                    ],
                ],
            ],
        ];
    }

    /**
     * @test
     */
    public function shouldRegisterANewTelegramUser()
    {
        $payload = $this->getPayload();

        $this->connectService->sendAccessLink($payload);

        $user = $this->em->getRepository('AppBundle:User')->findOneBy([
            'telegramId' => $payload['message']['from']['id']
        ]);

        $this->assertInstanceOf(User::class, $user);
    }

    /**
     * @test
     */
    public function shouldRegisterTheRequest()
    {
        $payload = $this->getPayload();

        $this->connectService->sendAccessLink($payload);

        $requests = $this->em->getRepository('AppBundle:ConnectRequest')->findAll();

        $this->assertCount(1, $requests);
        $this->assertEquals($payload['update_id'], $requests[0]->getUpdateId());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function shouldNotRegisterSameRequest()
    {
        $payload = $this->getPayload();
        $this->connectService->sendAccessLink($payload);
        $this->connectService->sendAccessLink($payload);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function shouldNotRegisterInvalidCommand()
    {
        $payload = $this->getPayload();
        $payload['message']['text'] = '/fail';
        $this->connectService->sendAccessLink($payload);
    }
}
