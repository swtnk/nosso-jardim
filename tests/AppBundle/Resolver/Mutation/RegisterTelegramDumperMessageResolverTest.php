<?php

use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use AppBundle\Resolver\Mutation\RegisterTelegramDumperMessageResolver;
use AppBundle\Entity\Message;
use AppBundle\Entity\MediaType;
use AppBundle\Entity\Media;
use AppBundle\Entity\User;

class RegisterTelegramDumperMessageResolverTest extends AbstractIntegrationTestCase
{
    /**
     * @var RegisterTelegramDumperMessageResolver
     */
    protected $resolver;

    static public $to = [
        'id'=>'$05000000a4122f3fa12d96f5ee4f4305',
        'peer_type'=>'channel',
        'peer_id'=>1060049572,
        'print_name'=>'PHP_DF',
        'flags'=>524289,
        'title'=>'PHP DF',
        'participants_count'=>0,
        'admins_count'=>0,
        'kicked_count'=>0
    ];

    protected function setUp()
    {
        parent::setUp();
        $this->resolver = $this->getService('app.resolver.telegramDumperMessages');
        
        $fixture = new LoadMediaTypeData();
        $fixture->load($this->em);
    }
    
    /**
     * @test
     */
    public function shouldRegisterATextMessage()
    {
        $bag = [
            'event'=> 'message',
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'text' => 'teste',
                'date' => time(),
            ]
        ];
        
        $message = $this->resolver->register($bag);
        
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNotNull($message->getId());
    }
    
    /**
     * @test
     */
    public function shouldRegisterAPhotoMessage()
    {
        $mediaDir = realpath(__DIR__ . '/../../../data');
        
        $bag = [
            'media_directory' => $mediaDir,
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'media' => [
                    'type' => 'photo',
                    'caption' => 'imagem de teste',
                    'file' => 'image.jpg'
                ],
                'date' => time()
            ]
        ];
        
        $message = $this->resolver->register($bag);
        
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNotNull($message->getId());
        $this->assertEquals('image.jpg', $message->getMedia()->getOriginalFilename());
        $this->assertNotEmpty($message->getMedia()->getFilename());
        $this->assertEquals(MediaType::PHOTO, $message->getMedia()->getType()->getId());
    }
    
    /**
     * @test
     */
    public function shouldRegisterAVideoMessage()
    {
        $mediaDir = realpath(__DIR__ . '/../../../data');
        
        $bag = [
            'media_directory' => $mediaDir,
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'media' => [
                    'type' => 'video',
                    'file' => 'video.mp4'
                ],
                'date' => time()
            ]
        ];
        
        $message = $this->resolver->register($bag);
        
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNotNull($message->getId());
        $this->assertEquals('video.mp4', $message->getMedia()->getOriginalFilename());
        $this->assertNotEmpty($message->getMedia()->getFilename());
        $this->assertEquals(MediaType::VIDEO, $message->getMedia()->getType()->getId());
    }
    
    /**
     * @test
     */
    public function shouldRegisterAnAudioMessage()
    {
        $mediaDir = realpath(__DIR__ . '/../../../data');
        
        $bag = [
            'media_directory' => $mediaDir,
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'media' => [
                    'type' => 'audio',
                    'file' => 'audio.mp3'
                ],
                'date' => time()
            ]
        ];
        
        $message = $this->resolver->register($bag);
        
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNotNull($message->getId());
        $this->assertEquals('audio.mp3', $message->getMedia()->getOriginalFilename());
        $this->assertNotEmpty($message->getMedia()->getFilename());
        $this->assertEquals(MediaType::AUDIO, $message->getMedia()->getType()->getId());
    }
    
    /**
     * @test
     */
    public function shouldRegisterADocumentMessage()
    {
        $mediaDir = realpath(__DIR__ . '/../../../data');
        
        $bag = [
            'media_directory' => $mediaDir,
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'media' => [
                    'type' => 'document',
                    'file' => 'document.pdf'
                ],
                'date' => time()
            ]
        ];
        
        $message = $this->resolver->register($bag);
        
        $this->assertInstanceOf(Message::class, $message);
        $this->assertNotNull($message->getId());
        $this->assertEquals('document.pdf', $message->getMedia()->getOriginalFilename());
        $this->assertNotEmpty($message->getMedia()->getFilename());
        $this->assertEquals(MediaType::DOCUMENT, $message->getMedia()->getType()->getId());
    }
    
    /**
     * @test
     */
    public function shouldCreateAnUser()
    {
        $bag = [
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 9876,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'text' => 'teste',
                'date' => time()
            ]
        ];
        
        $message = $this->resolver->register($bag);
       
        $sender = $message->getSender();
        
        $this->assertInstanceOf(User::class, $sender);
        $this->assertNotNull($sender->getId());
        $this->assertEquals(9876, $sender->getTelegramId());
    }
    
    /**
     * @test
     */
    public function shouldStoreUserPhoto()
    {
        $mediaDir = realpath(__DIR__ . '/../../../data');
        
        $bag = [
            'media_directory' => $mediaDir,
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 9876,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva',
                    'userPhoto' => 'profile.png'
                ],
                'to'=>self::$to,
                'text' => 'teste',
                'date' => time()
            ]
        ];
        
        $message = $this->resolver->register($bag);
       
        $sender = $message->getSender();
        
        $this->assertInstanceOf(Media::class, $sender->getPhoto());
        $this->assertEquals('profile.png', $sender->getPhoto()->getOriginalFilename());
        $this->assertNotEmpty($sender->getPhoto()->getFilename());
        $this->assertEquals(MediaType::PHOTO, $sender->getPhoto()->getType()->getId());
    }
    
    /**
     * @test
     */
    public function shouldNotCreateAnUserIfAlreadyExists()
    {
        $senderData = [
            'id' => 9876,
            'first_name' => 'Fulano',
            'last_name' => 'da Silva'
        ];
        
        $bag1 = [
            'json' => [
                'id' => '111',
                'from' => $senderData,
                'to'=>self::$to,
                'text' => 'primeira mensagem',
                'date' => time()
            ]
        ];
        
        $message1 = $this->resolver->register($bag1);
        
        $bag2 = [
            'json' => [
                'id' => '222',
                'from' => $senderData,
                'to'=>self::$to,
                'text' => 'segunda mensagem do mesmo usuario',
                'date' => time() + 1
            ]
        ];
        
        $message2 = $this->resolver->register($bag2);
       
        $sender1 = $message1->getSender();
        $sender2 = $message2->getSender();
        
        $this->assertEquals($sender1->getId(), $sender2->getId());
        $this->assertEquals($sender1->getTelegramId(), $sender2->getTelegramId());
        $this->assertEquals($sender1->getName(), $sender2->getName());
    }
    
    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function shouldNotRegisterMessageAlreadyImported()
    {
        $bag = [
            'json' => [
                'id' => '1',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'text' => 'primeira mensagem',
                'date' => time()
            ]
        ];
        $this->resolver->register($bag);
        $this->resolver->register($bag);
    }
    
    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function shouldNotRegisterMessageWithoutTextAndMedia()
    {
        $bag = [
            'json' => [
                'id' => '1',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'date' => time()
            ]
        ];
        
        $this->resolver->register($bag);
    }
    
    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function shouldNotRegisterMediaMessageWithoutMediaDirectory()
    {
        $bag = [
            'json' => [
                'id' => 'qwerty',
                'from' => [
                    'id' => 1,
                    'first_name' => 'Fulano',
                    'last_name' => 'da Silva'
                ],
                'to'=>self::$to,
                'media' => [
                    'type' => 'photo',
                    'caption' => 'imagem de teste',
                    'file' => 'image.jpg'
                ],
                'date' => time()
            ]
        ];
        $this->resolver->register($bag);
    }
}
