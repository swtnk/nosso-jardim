<?php
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Resolver\Mutation\RegisterTelegramDumperMessageResolver;
use AppBundle\Entity\Subtopic;

class SubtopicTest extends AbstractIntegrationTestCase
{


    protected function setUp()
    {
        parent::setUp();
        $this->em = $this->getService('doctrine.orm.entity_manager');
    }
    
    /**
     * @test
     */
    public function shouldCreateOne()
    {
        $subtopic = new Subtopic();
        $subtopic->setName('new one');
        $this->em->persist($subtopic);
        $this->em->flush();
        
        $repo = $this->em->getRepository('AppBundle\Entity\Subtopic');
        $subs = $repo->findAll();
        $this->assertCount(1,$subs);
    }
    
}
